package secondConnectingSwing;

import javax.swing.*;

public class Window2 {
    private JFrame frame;
    private JTextField textField;

    public Window2() {
        frame = new JFrame("Window2");
        frame.setBounds(550, 50, 400, 400);


        textField = new JTextField("Input2");
        textField.setBounds(0, 0, 400, 200);
        frame.add(textField);

        show(frame);
    }

    public String getInput() {
        return textField.getText();
    }

    public void show(JFrame frame) {
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
