package secondConnectingSwing;

import javax.swing.*;

public class Window1 {

    private JFrame frame;
    private JTextField textField;
    private JLabel label;
    private JButton button;
    private Window2 window2;

    public Window1(Window2 window2) {
        this.window2 = window2;

        frame = new JFrame("Window1");
        frame.setBounds(50, 50, 400, 600);

        textField = new JTextField("Input1");
        textField.setBounds(0, 0, 400, 200);
        frame.add(textField);

        label = new JLabel("Wpisz coś powyżej i kliknij aby połączyć dane z okienek");
        label.setBounds(50, 400, 400, 100);
        frame.add(label);

        button = new JButton("Połącz");
        button.setBounds(0, 400, 400, 200);
        button.addActionListener(e -> {
            String output = textField.getText() + window2.getInput();
            label.setText(output);
        });
        frame.add(button);

        show(frame);

    }

    public void show(JFrame frame) {
        frame.setLayout(null);
        frame.setVisible(true);

    }
}
