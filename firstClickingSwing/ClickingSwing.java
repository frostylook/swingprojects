package firstClickingSwing;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClickingSwing {
    public static void main(String[] args) {
        JFrame wow = new JFrame("Wow");
        wow.setSize (400, 250);
        wow.setLayout(null);

        JButton button = new JButton("Hello World");
        button.setBounds(100,50,150,100);

        button.addActionListener(new ActionListener() {
            int n = 0;
            @Override
            public void actionPerformed(ActionEvent e) {
                n++;
                button.setText("Clicked!" + n + "times");
            }
        });
        wow.add(button);
        wow.setVisible(true);
    }
}
