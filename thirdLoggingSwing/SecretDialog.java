package thirdLoggingSwing;

import javax.swing.*;
import java.awt.*;

public class SecretDialog {
    private JFrame frame;
    private JLabel label;

    public SecretDialog() {
        frame = new JFrame ("SecretWindow");
        frame.setLayout(null);
        frame.setBounds(50, 50, 400, 300);

        label = new JLabel ("You did it!");
        label.setBounds(0, 0, 400, 200);

        label.setFont(new Font(null, Font.BOLD | Font.ITALIC, 50));
        frame.add(label);

    }
    public void showSecretDialog(){
        frame.setVisible(true);
    }
}
